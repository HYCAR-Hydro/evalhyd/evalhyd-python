.. default-role:: obj

..
   latest
   ------

   Yet to be versioned and released. Only available from *dev* branch until then.

v0.1.2.0
--------

Released on 2024-01-22.

.. rubric:: Dependency changes

* move to `evalhyd-cpp==0.1.2`
  (`see changelog <https://hydrogr.github.io/evalhyd/cpp/changelog.html#v0-1-2>`_)

v0.1.1.0
--------

Released on 2023-06-16.

.. rubric:: Dependency changes

* move to `evalhyd-cpp==0.1.1`
  (`see changelog <https://hydrogr.github.io/evalhyd/cpp/changelog.html#v0-1-1>`_)

v0.1.0.0
--------

Released on 2023-05-03.

* first release
