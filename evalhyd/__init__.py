"""An evaluator for deterministic and probabilistic streamflow predictions."""

from .version import __version__
from .evald import evald
from .evalp import evalp
